""" Aspyanz Eksamnia

Examnia periodically reads Checkin Log:
    Receives checkin data direectly from  and compares readings against configured rules
    Outputs exceptions to Status Log:
        Threshold breaches
        New host checkins
        Hosts with no checkin for too long

                        +-------------+
                        | Config Repo |
                        |       __----
                        |____---
                               |
                               |
                               v
    +-----------+        +-----------+       +------------+
    | Logger    |=======>| Exksmnia  |------>| Status Log |
    +-----------+        +-----------+       |       __---
                                             |____---
                
"""
NODEID = "Site/1/Examnia/1"
import Config

eval_conf = Config.Config( NODEID )
