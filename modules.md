Aspyanz Modules
===============

 * jornal.py (journal)	Listens for incoming checkins and writes to journal files

 * eksamnya.py (examine)	Evaluates checkin info against rules 

 * messejer.py (messenger)	Sends alerts

 * kommondya.py (command)	CLI interface

 * rewlys.py (rules)	Manages rules in sections governing behaviour of modules


