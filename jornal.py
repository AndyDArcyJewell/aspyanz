""" jornal.py - Aspyanz Listener/Writer

 * accepts connections and reports (derivasow) from host reporter agents (maynoresow derivdoresow)
 * outputs all incoming reports to host journals (jornalys ostys)
 * logs brief details (datestamp, log, size) of connections and reports to host journals
 * uses rules (rewlys) in rules repository (kist rewlys) to determine:
    - basic behaviour
    - segregation of hosts within data store (e.g. by customer or site etc)

                                           
                          ┌─────────────────┐
                          │   kist_rewlys   │
                          │   (rules repo)  │
                          │                 │
                          │          ╭──────╯
                          └──────────╯
                                   │
                                   ↓               
  ┌─────────────────┐     ┌─────────────────┐     ┌─────────────────┐
  │  derividoresow  │┐    │    jornal       │     │    eksamnia     │
  │  (reporters)    ││===>│  (listener-     │====>│    (examine)    │
  │                 ││    │    writer)      │     │                 │
  └─────────────────┘│    └─────────────────┘     └─────────────────┘
   └─────────────────┘            │ │                
                                  │ │             
                       ┌──────────┘ └──────────┐   
                       │                       │   
                       v                       v   
              ┌─────────────────┐     ┌─────────────────┐
              │ report journal  │     │ host journals   │┐
              │                 │     │                 ││
              │                 │     │                 ││
              │         ╭───────╯     │         ╭───────╯│
              └─────────╯             └─────────╯╭───────╯
                                       └─────────╯  
    
"""

import collections
import time
import os
import re
import threading
import asyncio
#import uvloop


deque = collections.deque() # Queue of lists: ( fragments )

def write_journal( host_journal, report, time, host, address ): 
    """ Writes report to host_journal (file handle), logs the event to report journal,
        and forwards to eksamnia.
    
        host_journal :file_handle: to write to 
        report :string: report text to write 
        time :string: time string in format YYYYMMDD:hhmmss
        host :string: host name
    """

    host_journal.write( report )
    report_journal.write( "{s}\tjornal\treport from {s} on address {s}\n".format(
        time,host,address) )
    eksamnia_socket.write( report )


def open_journal( host, address ):
    """ Returns a journal file handle for (host,address) referring to a file path
        opened for appending, constructed after consulting placement and aging rules """
    # FIXME: interim stub that just joins the path elements together
    return open( os.path.join( journal_basedir, host + ".jnl" ), "a+" )

def writer_thread( deque ): 
    """ Accumulates packets from queue in the form of a tuple containing 
        (host,address,fragment) into a kist (box), until an "end 
        of report" marker is found, then merges the fragments and writes to the relevant jornal 
        ost (host journal), the writes a line to the jornal derivas (report journal), and passes
        the dervas (report) onto eksamnia (examine) for analysis.

        A special address "STOP" signifies that the thread should write all currently cached
        fragments, close all journal files, and exit.
        

        deque :collections.deque: queue of (address,packet) pairs
    """

    report_buff = {} # (host,address): [ handle, [ fragment, fragment, ... ] ]
    while True:
        while deque.count() > 0:
            host,address,fragment = deque.popleft() # get a packet
            if address == "STOP":
                break

            if (host,address) not in report_buff:
                host_journal_path = gen_journal_path( host, address )
                report_buff[(host,address)] = [ open_journal( host, address ), [ fragment ] ]
            else:
                if fragment == b"CLOSE":
                    # Write out anything left in the buffer and close the journal file...
                    write_journal(report_buff[(host,address)][0],
                                  b''.join(report_buff[(host,address)][1]), timestamp, address)
                    report_buff[(host,address)][0].close()
                    del report_buff[(host,address)]
                else:
                    # Just append this fragment to the buffer
                    report_buff[(host,address)][1].append(fragment)

            if re.search(b'.*END REPORT\n\n\s*$', fragment):
                write_journal(report_buff[(host,address)][0],
                              b''.join(report_buff[(host,address)][1]), timestamp, address)

    # Flush remaining buffered data and close journal files
    for host,buff in report_buff.items():
        handle,frags = buff
        if frags:
            handle.write(b''.join(frags))
        handle.close()

        

class CheckinProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        self.transport = transport
        self.peer_addr = transport.get_extra_info('peername')
        self.host_name = None # We don't know the hostname yet
        print("Connection from "+repr(self.peer_addr))

    def connection_lost(self, exc):

        # Signal writer to close this file handle
        deque.append((self.host_name,self.peer_addr,"CLOSE"))
        self.transport = None

    def data_received(self, data):
        """ First packet of data MUST be AT LEAST 38 bytes long or we disconnect """
        
        error = b"Protocol violation. Disconnecting!\n"
        if self.host_name is None: # Connection not yet initialised
            if  len(data) >= 38: # Shortest possible length for a valid C line
                # Should find hostname in first line of data
                # otherwise, drop connection with protocol violation
                firstline = data[:data.find(b"\r")]
                parts = firstline.split(b"\t")
                if len(parts) == 6 and (parts[1],parts[2],parts[4]) == (b"C",b"BEGIN",b"VERSION"):
                    self.host_name = parts[3]
                    print(b"host_name: "+self.host_name)
                    error = None

        if error:
            self.transport.write(error)
            self.transport.abort()
        else:                
            deque.append((self.host_name,self.peer_addr,data))

#loop = uvloop.new_event_loop()
loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
addr = ("0.0.0.0","2811")
srv = loop.run_until_complete( loop.create_server(CheckinProtocol, *addr) )
loop.run_until_complete(srv.wait_closed())
deque.append(("","STOP",""))

