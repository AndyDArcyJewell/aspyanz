Aspyanz - Infrastructure Resource Surveillance

System monitoring done differently.

The idea for Aspyanz came out of frustration with the limitations of existing monitoring systems, 
namely:

    * Performance - I/O, forking and scheduling
    * Expensive equipment required to run it
    * Disparate components with inconsistent user interfaces
    * Use of relational database or RRD  backends
    * Inflexibility of configuration of target hosts
    * Constant tweaking/management required
    * Complicated manual configuration or GUI-only configuration
    * Lack of RESTful data API's
    
Here's the Big Plan:

    * Everything is logged
        - each host has it's own log
        - segmented logs for easy maintenance
        - automatic deletion of aged log segments
        - no loss of resolution with age (unless desired)
        - programmable "thinning" of aged log data 
        - new data sources immediately accessible 
        - easily replicatable with rsync or zfs send
    * Everything is graphable
        - check-in times
        - resource constraint changes
        - threshold changes
    * Minimal I/O debt 
        - no temporary files
        - no cascading updates
        - no database cleanup jobs
        - logs are append-only
        - checkin data goes straight into logs and stays there
        - no un-necessary closing and re-opening of files
        - query results are cached to reduce un-neccesary file scans
    * Modular, robust, scalable architecture
        - run everything on one server if you like
        - or spread it amongst multiple hosts
    * Designed to be able to monitor hundreds of targets using an old laptop
    * Native OS Push agents 
        - "check in" with listener
        - self-scheduling
        - report only: no descision re: good/bad status
    * Active check poller modules for compatability with existing systems
        - SNMP
        - Nagios NRPE
        - Check_MK
        - HTTP/FTP etc.
        - SSH
          

Main Modules:
    * Agent 
    * Listener (goslwyas)
    * Reporter (derivadores)
    * Alerter (messejer)
    

Written in Python 3.5 using Async IO where appropriate.
