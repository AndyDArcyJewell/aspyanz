""" Aspyanz rewlys (rules)

Inheritance-based config repository interface.

All processes read their configuration from the Config repository at regular intervals (config_refresh_interval)

This module contains an interface to allow each processes to read its configuration.

Inheritance: repositories deeper in the tree inherit settings from all nodes along their path, by loading the corresponding
config file with the same base name as the directory:
    e.g. root/org1 --> root/org1.cfg
    
Keys with the same name in more deeply nested directories over-ride those in less deeply nested directories:

Values defined at each level:
                                key1    key2    key3
    root                        1       x        
    root/org1.cfg                                
    root/org1/site1.cfg                 y       a
    root/org1/site2.cfg                 z       b
    root/org2.cfg                                
    root/org2/site1.cfg                 u       c
    root/org2/site2.cfg                 v       d
    
Resulting inherited values at each point
                                key1    key2    key3
    root                        1       x        
    root/org1                   1       x        
    root/org1/site1             1       y       a
    root/org1/site2             1       z       b
    root/org2                   1       x        
    root/org2/site1             1       u       c
    root/org2/site2             1       v       d


Example:
                                            root----root.cfg
       ......................................|            
       :                  :                  |             
     org1----org1.cfg   org2----org2.cfg   org3----org3.cfg
       :                  :                  |....................
                                             |                    :
                                            site1---site1.cfg   site2---site2.cfg  site3
                                   ___________|.......
                                  |                   :
                                log1----log1.cfg    log2----log2.cfg
                                        
The path "root/org3/site1/log1" will load:
    root.cfg
    root/org3.cfg
    root/org3/site1.cfg
    root/org3/site1/log1.cfg
"""


import os
import yaml
import collections

class Config(collections.MutableMapping):

    def _load_all_config( self, filename ):
        print("_load_all_config:Loading config file:", filename)
        full_filename = "/".join( ( self.config_root, self.config_repository,  filename ) )
        print("_load_all_config: full_filename:",full_filename)
        if os.path.exists( os.path.dirname( full_filename ) + ".cfg" ):
            contents = yaml.load( open( os.path.dirname( full_filename ) + ".cfg" ) ) 
            self.store.update( contents ) 
        if os.path.isdir( full_filename ):
            for entry in os.listdir( full_filename ):             
                print("_load_all_config: recursing into _load_all_config(%s)" %    "/".join( (filename, entry ) ) )
                self._load_all_config( "/".join( (filename, entry ) ) )
        else:
            self.store.update( yaml.load( open(full_filename) ) )

    def _load_parents( self ):
        """ loads and merges config files along filename's path 
            from root onwards, such that:
            
            config = root || great-grandparent || grandparent || parent || node
        """
        for parent in [ self.config_repository[:sl] for sl,ch in enumerate(self.config_repository) 
                        if sl>0 and ch == "/" ]:
            print( "_load_parents:Loading", parent, "..." )
            parent_cfg = parent + ".cfg"
            print( "_load_parents:Looking for file:", parent_cfg, )
            if os.path.exists( self.config_root + parent_cfg ):
                print( "_load_parents:Loading file:", parent_cfg, )
                contents = self._load_file( parent_cfg )
                print( "_load_parents:    Contents:", contents )
                self.store.update( contents  ) 

        
    def _load_file( self, filename ):
        load = yaml.load( open(self.config_root + "/" + filename) )
        return load

    def __init__( self, etc_file ):
        """ Initilises object and loads entire config tree into it.
        
            etc_file is expected to be a yaml document as follows:
            
            ########### still to be decided #############
            Config:
                ConfigRoot:    /var/lib/Aspyanz/config/
                Site:
                    Name:   SiteA
                    Nodes:      
                Logger:
                    site_name:      site1
                    node_name:      node1
                
            from this, the repository to load the dynamic configuration is determined
        """        

        etc_conf = yaml.load( open( etc_file ) )
        self.config_root = etc_conf['Config']['ConfigRoot']
        self.config_repository = "/" + etc_conf['Config']['Repository'] 
        print("Repository:",self.config_repository)
        self.config_log_name = self.config_repository + ".log"
        self.store = {}
        self._load_parents()
        self._load_all_config( '' )
        try:
            self.config_log_stat = os.stat( self.config_root + "/" +self.config_log_name )
        except OSError:
            self.config_log_stat = os.stat( self.config_root )
            
    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        self.store[key] = value

    def __delitem__(self, key):
        del self.store[key]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def reload( self ):
        """ Reloads config if it's log file has changed, we re-open it and seek to last known eof, and 
            read all subsequent config lines, and remember our current position. Then we re-read all
            configuration files mentioned in the new entries in the log.
        """
        if os.stat( self.config_log_name ).st_mtime > self.config_log_stat.st_mtime:
            # Try to avoid race condition where log file is updated while we read it:
            # After opening the file, we take a snapshot of the stats and only read 
            # from our last position, up to the end of file we initially saw.
            # Then we remember where we read up to. Next call we will pick up the rest.
            config_log_handle = open( self.config_log_name, "r" )
            new_stat = os.fstat( config_log_handle.fileno() )
            config_log_handle.seek( self.config_log_stat.st_size, 0 )
            new_lines = config_log_handle.read( new_stat.st_size - self.config_log_stat.st_size )
            config_log_handle.close()
            self.config_log_stat = new_stat
            # Strip timestamp and remove any duplicate file entries from log (only need to read files once)
            new_lines = set( logline[15:] for logline in new_lines.splitlines() )
            changes = [ (fn,self._load_file(fn)) for fn in new_lines ]
            self.store.update( changes )
            
