""" Aspyanz Messejer (Alerter)

Messejer reads Status Log:
    Generates alerts according to configuration
    
                        +-------------+
                        | Kist Rewlys |
                        |       __----
                        |____---
                               |
                               |
                               v    
    +-------------+      +-----------+       +--------------+
    | Jornal      +----->| Messejer  |------>| Jornal       |
    | Statys__----|      +-----------+       | Messajys___---
    |____--- __---             |             |____-----                             
     |____---                  |
                               v
                        +-------------+
                        | Messajys    ||
                        |       __---- |
                        |____--- __----
                         |____---

    Jornal Statys       Status Journal
    Messejer            Messenger
    Messajys            Messages
    Jornal Messajys     Message Journal
    Kist Rewlys         Rules box (repository)

                         
"""

NODEID = "Site/1/Messejer/1"
import Config

alerter_conf = Config.Config( NODEID )
